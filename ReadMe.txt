1.Building the application
————————————————————————-
Prerequisites Maven 3+, Java 7

Extract the zip file

1. From the console, CD to nabtest
2. run maven clean install

   The following activities are done during the build process.

	1. Executes all unit test cases.
	2. Creates h2 data base
	3. Starts jetty server
	4. Executes Integrations test cases by hitting the CRUD restful service.

2.Running the rest service and testing the service manually
—————————————————

1. CD to nabtest
2. execute maven clean install
3. CD to nabtest-web
4. execute maven -P JETTY jetty:run
5. The REST service starts up


List of sample request and response, Please use a rest client.(I have used a Chrome extension called Advanced Rest Client)

1. Create user - This is a POST request.
	
Request
——
URL->http://localhost:8080/test/create

Please specify the below POST parameter 

data.json = {"firstName":"fname22","middleName":"mname3","surname":"sname","initials":"Mr.","title":"Eng.","sex":"M","maritalStatus":"Married","creditRating":"100","address":{"id":0,"customerId":0,"addressLine1":"line1","addressLine2":"line2","streetName":null,"suburb":null,"city":"city","state":"state","country":"country","pinCode":"12345678"},"nabcustomer":false}

Response
———-
“Success” or “Failure” with respective http status codes


2. Update user - This is a POST request.

Request
———-

URL->http://localhost:8080/test/update

Please specify the below POST parameter (and set the customerId in JSON string, in this case it is 1 )

data.json = {"firstName":"fname22","customerId":"1","middleName":"mname3","surname":"sname","initials":"Mr.","title":"Eng.","sex":"M","maritalStatus":"Married","creditRating":"100","address":{"id":0,"customerId":0,"addressLine1":"line1","addressLine2":"line2","streetName":null,"suburb":null,"city":"city","state":"state","country":"country","pinCode":"12345678"},"nabcustomer":false}

Response
———-
“Success” or “Failure” with respective http status codes



3. Get user - This is a GET request

Request
———
URL - http://localhost:8080/test/get/1 last part of the url is customer id to be retrieved.

Response 
————
{"firstName":"fname22","middleName":"mname3","surname":"sname","initials":"Mr.","title":"Eng.","sex":"M","maritalStatus":"Married","creditRating":"100","customerId":1,"address":{"id":2,"customerId":1,"addressLine1":"line1","addressLine2":"line2","streetName":null,"suburb":null,"city":"city","state":"state","country":"country","pinCode":"12345678"},"nabcustomer":false}

4. Delete user - This is a DELETE request.

Request
———
url - > http://localhost:8080/test/delete/1 last part of the url is customer id to be deleted.

Response
———-

“Success” or “Failure” with respective http status codes







