package task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.sql.ResultSet;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CustomerRSExtractor.class,Customer.class,Address.class,CustomerRSExtractorTest.class})
public class CustomerRSExtractorTest {

    @Test
    public void testExtractData() throws Exception {

        CustomerRSExtractor customerRSExtractor = new CustomerRSExtractor();

        ResultSet mockResultSet = mock(ResultSet.class);
        Customer mockCustomer = mock(Customer.class);
        Address mockAddress = mock(Address.class);

        PowerMockito.whenNew(Customer.class).withNoArguments().thenReturn(mockCustomer);
        PowerMockito.whenNew(Address.class).withNoArguments().thenReturn(mockAddress);



        when(mockResultSet.getString(1)).thenReturn("1");
        when(mockResultSet.getString(2)).thenReturn("INI");
        when(mockResultSet.getString(3)).thenReturn("TITLE");
        when(mockResultSet.getString(4)).thenReturn("FNAME");
        when(mockResultSet.getString(5)).thenReturn("MNAME");
        when(mockResultSet.getString(6)).thenReturn("SNAME");
        when(mockResultSet.getString(7)).thenReturn("MALE");
        when(mockResultSet.getString(8)).thenReturn("SINGLE");
        when(mockResultSet.getString(9)).thenReturn("100");
        when(mockResultSet.getString(10)).thenReturn("TRUE");
        when(mockResultSet.getString(11)).thenReturn("1");
        when(mockResultSet.getString(12)).thenReturn("1");
        when(mockResultSet.getString(13)).thenReturn("ADDR1");
        when(mockResultSet.getString(14)).thenReturn("ADDR2");
        when(mockResultSet.getString(15)).thenReturn("STREET");
        when(mockResultSet.getString(16)).thenReturn("SURB");
        when(mockResultSet.getString(17)).thenReturn("CITY");
        when(mockResultSet.getString(18)).thenReturn("STATE");
        when(mockResultSet.getString(19)).thenReturn("CON");
        when(mockResultSet.getString(20)).thenReturn("PIN");


        customerRSExtractor.extractData(mockResultSet);


        verify(mockResultSet).next();
        verify(mockResultSet).getString(1);
        verify(mockResultSet).getString(2);
        verify(mockResultSet).getString(3);
        verify(mockResultSet).getString(4);
        verify(mockResultSet).getString(5);
        verify(mockResultSet).getString(6);
        verify(mockResultSet).getString(7);
        verify(mockResultSet).getString(8);
        verify(mockResultSet).getString(9);
        verify(mockResultSet).getString(10);
        verify(mockResultSet).getString(11);
        verify(mockResultSet).getString(12);
        verify(mockResultSet).getString(13);
        verify(mockResultSet).getString(14);
        verify(mockResultSet).getString(15);
        verify(mockResultSet).getString(16);
        verify(mockResultSet).getString(17);
        verify(mockResultSet).getString(18);
        verify(mockResultSet).getString(19);
        verify(mockResultSet).getString(20);


        verify(mockCustomer).setAddress(mockAddress);
        verify(mockCustomer).setFirstName("FNAME");
        verify(mockCustomer).setMiddleName("MNAME");
        verify(mockCustomer).setSurname("SNAME");
        verify(mockCustomer).setInitials("INI");
        verify(mockCustomer).setTitle("TITLE");
        verify(mockCustomer).setSex("MALE");
        verify(mockCustomer).setMaritalStatus("SINGLE");
        verify(mockCustomer).setCreditRating("100");
        verify(mockCustomer).setCustomerId(1L);
        verify(mockCustomer).setNABCustomer(true);

        verify(mockAddress).setAddressLine1("ADDR1");
        verify(mockAddress).setAddressLine2("ADDR2");
        verify(mockAddress).setStreetName("STREET");
        verify(mockAddress).setCity("CITY");
        verify(mockAddress).setState("STATE");
        verify(mockAddress).setCountry("CON");
        verify(mockAddress).setPinCode("PIN");
        verify(mockAddress).setId(1L);
        verify(mockAddress).setCustomerId(1L);
        verify(mockAddress).setSuburb("SURB");



        verifyNoMoreInteractions(mockResultSet,mockCustomer,mockAddress);


    }
}