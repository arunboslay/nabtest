package task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({JdbcTemplate.class, DefaultCRUDRepository.class, DefaultCRUDRepositoryTest.class, Object[].class})
public class DefaultCRUDRepositoryTest {


    public static final String CR = "100";
    public static final long ID = 1L;
    public static final String FNAME = "fname";
    public static final String MNAME = "mname";
    public static final String SNAME = "sname";
    public static final String MR = "Mr";
    public static final String ENG = "Eng";
    public static final String SINGLE = "Single";
    public static final String MALE = "M";

    @Test
    public void testCreateCustomer() throws Exception {

        DataSource mockDataSource = mock(DataSource.class);

        JdbcTemplate mockJdbcTemplateSeq = mock(JdbcTemplate.class);
        JdbcTemplate mockJdbcTemplateInsert = mock(JdbcTemplate.class);

        PowerMockito.whenNew(JdbcTemplate.class).withArguments(mockDataSource).thenReturn(mockJdbcTemplateSeq,mockJdbcTemplateInsert,mockJdbcTemplateSeq);

        when(mockJdbcTemplateSeq.queryForObject(anyString(), any(Object[].class), any(Class.class))).thenReturn("0");

        //when(mockJdbcTemplate.update(CRUDRepository.UPDATE_SEQ_GEN, eq(new Object[]{1l})  )    ).thenReturn()

        CRUDRepository crudRepository = new DefaultCRUDRepository(mockDataSource);


        Customer mockCustomer = mock(Customer.class);
        when(mockCustomer.getCreditRating()).thenReturn(CR);
        when(mockCustomer.getCustomerId()).thenReturn(ID);
        when(mockCustomer.getFirstName()).thenReturn(FNAME);
        when(mockCustomer.getMiddleName()).thenReturn(MNAME);
        when(mockCustomer.getSurname()).thenReturn(SNAME);
        when(mockCustomer.getInitials()).thenReturn(MR);
        when(mockCustomer.getTitle()).thenReturn(ENG);
        when(mockCustomer.getMaritalStatus()).thenReturn(SINGLE);
        when(mockCustomer.getSex()).thenReturn(MALE);
        when(mockCustomer.isNABCustomer()).thenReturn(true);

        Address  mockAddress = mock(Address.class);
        when(mockCustomer.getAddress()).thenReturn(mockAddress);

        when(mockAddress.getCustomerId()).thenReturn(ID);
        when(mockAddress.getAddressLine1()).thenReturn("ADDR1");
        when(mockAddress.getAddressLine2()).thenReturn("ADDR2");
        when(mockAddress.getCity()).thenReturn("CITY");
        when(mockAddress.getState()).thenReturn("STATE");
        when(mockAddress.getCountry()).thenReturn("COUNTRY");
        when(mockAddress.getPinCode()).thenReturn("1234");
        when(mockAddress.getStreetName()).thenReturn("SNAME");
        when(mockAddress.getId()).thenReturn(ID);




        crudRepository.createCustomer(mockCustomer);

        verify(mockJdbcTemplateSeq,times(2)).update(eq(DefaultCRUDRepository.UPDATE_SEQ_GEN), any(Object[].class));

        verify(mockJdbcTemplateInsert).update(DefaultCRUDRepository.INSERT_CUSTOMER,
                ID,
                MR,
                ENG,
                FNAME,
                MNAME,
                SNAME,
                MALE,
                SINGLE,
                CR,
                true);


        verify(mockCustomer).getAddress();
        verify(mockCustomer).getCreditRating();
        verify(mockCustomer).getFirstName();
        verify(mockCustomer).getMiddleName();
        verify(mockCustomer).getSurname();
        verify(mockCustomer).getInitials();
        verify(mockCustomer).getTitle();
        verify(mockCustomer).getMaritalStatus();
        verify(mockCustomer).getSex();
        verify(mockCustomer).isNABCustomer();

        verify(mockAddress).getAddressLine1();
        verify(mockAddress).getAddressLine2();
        verify(mockAddress).getCity();
        verify(mockAddress).getCountry();
        //verify(mockAddress).getCustomerId();
//        verify(mockAddress).getId();
        verify(mockAddress).getPinCode();
        verify(mockAddress).getState();
        verify(mockAddress).getStreetName();
        verify(mockAddress).getSuburb();

        verify(mockJdbcTemplateSeq, times(2)).queryForObject(DefaultCRUDRepository.QUERY_SEQ_GEN,new Object[]{}, String.class);

        verify(mockJdbcTemplateInsert).update(DefaultCRUDRepository.INSERT_ADDRESS, 1L,
                1L,
                "ADDR1",
                "ADDR2",
                "SNAME",
                null,
                "CITY",
                "STATE",
                "COUNTRY",
                "1234");



        verifyNoMoreInteractions(mockCustomer,mockAddress,mockDataSource, mockJdbcTemplateSeq);



    }


    @Test
    public void testUpdateCustomer() throws Exception{
        Customer mockCustomer = mock(Customer.class);
        when(mockCustomer.getCreditRating()).thenReturn(CR);
        when(mockCustomer.getCustomerId()).thenReturn(ID);
        when(mockCustomer.getFirstName()).thenReturn(FNAME);
        when(mockCustomer.getMiddleName()).thenReturn(MNAME);
        when(mockCustomer.getSurname()).thenReturn(SNAME);
        when(mockCustomer.getInitials()).thenReturn(MR);
        when(mockCustomer.getTitle()).thenReturn(ENG);
        when(mockCustomer.getMaritalStatus()).thenReturn(SINGLE);
        when(mockCustomer.getSex()).thenReturn(MALE);
        when(mockCustomer.isNABCustomer()).thenReturn(true);

        Address  mockAddress = mock(Address.class);
        when(mockCustomer.getAddress()).thenReturn(mockAddress);

        when(mockAddress.getCustomerId()).thenReturn(ID);
        when(mockAddress.getAddressLine1()).thenReturn("ADDR1");
        when(mockAddress.getAddressLine2()).thenReturn("ADDR2");
        when(mockAddress.getCity()).thenReturn("CITY");
        when(mockAddress.getState()).thenReturn("STATE");
        when(mockAddress.getCountry()).thenReturn("COUNTRY");
        when(mockAddress.getPinCode()).thenReturn("1234");
        when(mockAddress.getStreetName()).thenReturn("SNAME");
        when(mockAddress.getId()).thenReturn(ID);

        DataSource mockDataSource = mock(DataSource.class);

        JdbcTemplate mockJdbcTemplateUpdate = mock(JdbcTemplate.class);

        PowerMockito.whenNew(JdbcTemplate.class).withArguments(mockDataSource).thenReturn(mockJdbcTemplateUpdate);

        CRUDRepository crudRepository = new DefaultCRUDRepository(mockDataSource);

        crudRepository.updateCustomer(mockCustomer);


        verify(mockCustomer).getAddress();
        verify(mockCustomer).getCreditRating();
        verify(mockCustomer).getFirstName();
        verify(mockCustomer).getMiddleName();
        verify(mockCustomer).getSurname();
        verify(mockCustomer).getInitials();
        verify(mockCustomer).getTitle();
        verify(mockCustomer).getMaritalStatus();
        verify(mockCustomer).getSex();
        verify(mockCustomer).isNABCustomer();
        verify(mockCustomer).getCustomerId();

        verify(mockAddress).getAddressLine1();
        verify(mockAddress).getAddressLine2();
        verify(mockAddress).getCity();
        verify(mockAddress).getCountry();

        verify(mockAddress).getId();
        verify(mockAddress).getPinCode();
        verify(mockAddress).getState();
        verify(mockAddress).getStreetName();
        verify(mockAddress).getSuburb();

        verify(mockJdbcTemplateUpdate).update(DefaultCRUDRepository.UPDATE_CUSTOMER, "Mr",
                "Eng",
                "fname",
                "mname",
                "sname",
                "M",
                "Single",
                "100",
                true,
                1L);
        verify(mockJdbcTemplateUpdate).update(DefaultCRUDRepository.UPDATE_ADDRESS, "ADDR1",
                "ADDR2",
                "SNAME",
                null,
                "CITY",
                "STATE",
                "COUNTRY",
                "1234",
                1L);

        verifyNoMoreInteractions(mockCustomer,mockAddress,mockDataSource, mockJdbcTemplateUpdate);

    }

@Test
    public void testDelete()throws Exception{

        DataSource mockDataSource = mock(DataSource.class);

        JdbcTemplate mockJdbcTemplateUpdate = mock(JdbcTemplate.class);

        PowerMockito.whenNew(JdbcTemplate.class).withArguments(mockDataSource).thenReturn(mockJdbcTemplateUpdate);
        CRUDRepository crudRepository = new DefaultCRUDRepository(mockDataSource);

        crudRepository.deleteUser("1");
        verify(mockJdbcTemplateUpdate).update(DefaultCRUDRepository.DELETE_CUSTOMER,"1");

        verify(mockJdbcTemplateUpdate).update(DefaultCRUDRepository.DELETE_ADDRESS,"1");
    }


    @Test
    public void testGetCustomer() throws Exception{

        DataSource mockDataSource = mock(DataSource.class);

        JdbcTemplate mockJdbcTemplateUpdate = mock(JdbcTemplate.class);


        CustomerRSExtractor mockCustomerResultSetExtractor = mock(CustomerRSExtractor.class);
        PowerMockito.whenNew(CustomerRSExtractor.class).withNoArguments().thenReturn(mockCustomerResultSetExtractor);
        PowerMockito.whenNew(JdbcTemplate.class).withArguments(mockDataSource).thenReturn(mockJdbcTemplateUpdate);

        CRUDRepository crudRepository = new DefaultCRUDRepository(mockDataSource);

        crudRepository.getCustomer("1");

        verify(mockJdbcTemplateUpdate).query(DefaultCRUDRepository.SELECT_CUSTOMER,mockCustomerResultSetExtractor,"1" );
    }

}