package task;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class DefaultCRUDServiceTest {

    public static final String CUST_ID = "custId";

    @Test
    public void testCreateCustomer() throws Exception, CRUDServiceException {

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);

        Customer mockCustomer = mock(Customer.class);

        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        defaultCRUDService.createCustomer(mockCustomer);

        verify(mockcrudRepository).createCustomer(mockCustomer);
        verifyNoMoreInteractions(mockcrudRepository,mockCustomer);

    }

    @Test
    public void testCreateCustomerThrowsException() throws Exception{

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);

        Customer mockCustomer = mock(Customer.class);
        doThrow(new RuntimeException()).when(mockcrudRepository).createCustomer(mockCustomer);

        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        try {
            defaultCRUDService.createCustomer(mockCustomer);
            fail("Exception should have thrown");
        } catch (CRUDServiceException e) {

            verify(mockcrudRepository).createCustomer(mockCustomer);
        }

        verifyNoMoreInteractions(mockcrudRepository,mockCustomer);
    }

    @Test
    public void testUpdateCustomer() throws Exception, CRUDServiceException {

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);

        Customer mockCustomer = mock(Customer.class);


        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        defaultCRUDService.updateCustomer(mockCustomer);

        verify(mockcrudRepository).updateCustomer(mockCustomer);
        verifyNoMoreInteractions(mockcrudRepository,mockCustomer);

    }

    @Test
    public void testUpdateCustomerThrowsException() throws Exception{

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);

        Customer mockCustomer = mock(Customer.class);
        doThrow(new RuntimeException()).when(mockcrudRepository).updateCustomer(mockCustomer);

        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        try {
            defaultCRUDService.updateCustomer(mockCustomer);
            fail("Exception should have thrown");
        } catch (CRUDServiceException e) {

            verify(mockcrudRepository).updateCustomer(mockCustomer);
        }

        verifyNoMoreInteractions(mockcrudRepository,mockCustomer);
    }

    @Test
    public void testDeleteCustomer() throws Exception, CRUDServiceException {

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);


        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        defaultCRUDService.deleteUser(CUST_ID);

        verify(mockcrudRepository).deleteUser(CUST_ID);
        verifyNoMoreInteractions(mockcrudRepository);

    }

    @Test
    public void testDeleteCustomerThrowsException() throws Exception{

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);


        doThrow(new RuntimeException()).when(mockcrudRepository).deleteUser(CUST_ID);

        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        try {
            defaultCRUDService.deleteUser(CUST_ID);
            fail("Exception should have thrown");
        } catch (CRUDServiceException e) {

            verify(mockcrudRepository).deleteUser(CUST_ID);
        }

        verifyNoMoreInteractions(mockcrudRepository);
    }

    @Test
    public void testGetCustomer() throws Exception, CRUDServiceException {

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);


        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        defaultCRUDService.getCustomer(CUST_ID);

        verify(mockcrudRepository).getCustomer(CUST_ID);
        verifyNoMoreInteractions(mockcrudRepository);

    }

    @Test
    public void testGetCustomerThrowsException() throws Exception{

        CRUDRepository mockcrudRepository = mock(CRUDRepository.class);


        doThrow(new RuntimeException()).when(mockcrudRepository).getCustomer(CUST_ID);

        DefaultCRUDService defaultCRUDService = new DefaultCRUDService(mockcrudRepository);

        try {
            defaultCRUDService.getCustomer(CUST_ID);
            fail("Exception should have thrown");
        } catch (CRUDServiceException e) {

            verify(mockcrudRepository).getCustomer(CUST_ID);
        }

        verifyNoMoreInteractions(mockcrudRepository);
    }

}