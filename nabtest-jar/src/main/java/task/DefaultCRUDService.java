package task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DefaultCRUDService implements CRUDService {

    private CRUDRepository crudRepository;

    @Autowired
    public DefaultCRUDService(CRUDRepository crudRepository){

        this.crudRepository = crudRepository;

    }

    /**
     *
     * @param customer
     * @return
     * @throws CRUDServiceException
     */
    @Transactional
    public String createCustomer(Customer customer) throws CRUDServiceException {

        try{
          return  crudRepository.createCustomer(customer);

        }catch (Exception e){

            throw  new CRUDServiceException(e);
        }

    }

    /**
     *
     * @param id
     * @return
     * @throws CRUDServiceException
     */
    public Customer getCustomer(String id) throws CRUDServiceException {

        try{
            return   crudRepository.getCustomer(id);

        }catch (Exception e){

            throw  new CRUDServiceException(e);
        }

    }

    /**
     *
     * @param customer
     * @throws CRUDServiceException
     */
    @Transactional
    public void updateCustomer(Customer customer) throws CRUDServiceException{

        try{
            crudRepository.updateCustomer(customer);

        }catch (Exception e){

            throw  new CRUDServiceException(e);
        }

    }

    /**
     *
     * @param id
     * @throws CRUDServiceException
     */
    @Transactional
    public void deleteUser(String id)throws CRUDServiceException{

        try{
            crudRepository.deleteUser(id);

        }catch (Exception e){

            throw  new CRUDServiceException(e);
        }

    }

}