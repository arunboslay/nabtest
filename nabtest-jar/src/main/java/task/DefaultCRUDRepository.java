package task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class DefaultCRUDRepository implements CRUDRepository {

    private DataSource dataSource;

    public static final String DELETE_ADDRESS = "DELETE FROM NAB.ADDRESS WHERE CUSTOMER_ID = ?";
    public static final String DELETE_CUSTOMER = "DELETE FROM NAB.CUSTOMER WHERE ID = ?";
    public static final String UPDATE_SEQ_GEN = "UPDATE NAB.SEQ_GEN SET SEQ_GEN=?";
    public static final String QUERY_SEQ_GEN = "SELECT SEQ_GEN FROM NAB.SEQ_GEN";
    public static final String INSERT_ADDRESS = "INSERT INTO NAB.ADDRESS VALUES(?,?,?,?,?,?,?,?,?,?)";
    public static final String INSERT_CUSTOMER = "INSERT INTO NAB.CUSTOMER VALUES (?,?,?,?,?,?,?,?,?,? )";
    public static final String UPDATE_CUSTOMER = "UPDATE NAB.CUSTOMER SET INITIALS=?," +
            "TITLE=?," +
            "FIRST_NAME=?," +
            "MIDDLE_NAME=?," +
            "LAST_NAME=?," +
            "SEX=?," +
            "MARITAL_STATUS=?," +
            "CREDIT_RATING=?," +
            "IS_NAB_CUSTOMER=? WHERE ID=?";
    public static final String UPDATE_ADDRESS = "UPDATE NAB.ADDRESS SET " +
            "ADDRESS_LINE_1=?," +
            "ADDRESS_LINE_2=?," +
            "STREET_NAME=?," +
            "SUBURB=?," +
            "CITY=?," +
            "STATE=?," +
            "COUNTRY=?," +
            "PIN_CODE=? WHERE ID =?";
    public static final String SELECT_CUSTOMER = "SELECT * FROM NAB.CUSTOMER cus,NAB.ADDRESS addr WHERE cus.ID=addr.CUSTOMER_ID AND cus.ID =?";

    @Autowired
    public DefaultCRUDRepository(DataSource dbDataSource) {
        this.dataSource = dbDataSource;

    }

    /**
     *
     * @param customer
     * @return
     */

    public String createCustomer(Customer customer) {

        long customerId = generateSequence();

        Object[] insertParams = new Object[]{customerId,
                customer.getInitials(),
                customer.getTitle(), customer.getFirstName(),
                customer.getMiddleName(), customer.getSurname(),
                customer.getSex(), customer.getMaritalStatus(), customer.getCreditRating(), customer.isNABCustomer()};

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(INSERT_CUSTOMER, insertParams);

        Address address = customer.getAddress();
        Object[] addressParams = new Object[]{generateSequence(), customerId, address.getAddressLine1(),

                address.getAddressLine2(), address.getStreetName(), address.getSuburb(),
                address.getCity(), address.getState(), address.getCountry(),
                address.getPinCode()
        };

        jdbcTemplate.update(INSERT_ADDRESS, addressParams);

        return "" + customerId;

    }

    /**
     *
     * @param customer
     */
    public void updateCustomer(Customer customer) {

        Object[] insertParams = new Object[]{
                customer.getInitials(),
                customer.getTitle(), customer.getFirstName(),
                customer.getMiddleName(), customer.getSurname(),
                customer.getSex(), customer.getMaritalStatus(), customer.getCreditRating(), customer.isNABCustomer(),
                customer.getCustomerId()};

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        jdbcTemplate.update(UPDATE_CUSTOMER, insertParams);

        Address address = customer.getAddress();
        Object[] addressParams = new Object[]{address.getAddressLine1(),

                address.getAddressLine2(), address.getStreetName(), address.getSuburb(),

                address.getCity(), address.getState(), address.getCountry(),
                address.getPinCode(), address.getId()};

        jdbcTemplate.update(UPDATE_ADDRESS, addressParams);

    }

    /**
     *
     * @param customerId
     * @return
     */
    public Customer getCustomer(String customerId) {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        return jdbcTemplate.query(SELECT_CUSTOMER, new CustomerRSExtractor(), new Object[]{customerId});
    }

    /**
     *
      * @param customerId
     */
    public void deleteUser(String customerId) {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update(DELETE_ADDRESS, new Object[]{customerId});
        jdbcTemplate.update(DELETE_CUSTOMER, new Object[]{customerId});
    }

    /**
     *
     * @return
     */
    public long generateSequence() {

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        String strId = (String) jdbcTemplate.queryForObject(
                QUERY_SEQ_GEN, new Object[]{}, String.class);

        long id = Long.parseLong(strId);

        jdbcTemplate.update(UPDATE_SEQ_GEN, new Object[]{++id});

        return id;
    }

}
