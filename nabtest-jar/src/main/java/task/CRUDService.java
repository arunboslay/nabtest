package task;

import org.springframework.transaction.annotation.Transactional;

/**
 * Created by kannan on 19/01/2016.
 */
public interface CRUDService {
    @Transactional
    String createCustomer(Customer customer) throws CRUDServiceException;

    Customer getCustomer(String id) throws CRUDServiceException;

    void updateCustomer(Customer customer) throws CRUDServiceException;

    void deleteUser(String id) throws CRUDServiceException;
}
