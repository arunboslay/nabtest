package task;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerRSExtractor implements ResultSetExtractor<Customer>{

    /**
     *
     * @param rs
     * @return
     * @throws SQLException
     * @throws DataAccessException
     */
    public Customer extractData(ResultSet rs) throws SQLException, DataAccessException {
        rs.next();

        Customer customer = new Customer();

        customer.setCustomerId(Long.parseLong(rs.getString(1)));

        customer.setInitials(rs.getString(2));
        customer.setTitle(rs.getString(3));

        customer.setFirstName(rs.getString(4));

        customer.setMiddleName(rs.getString(5));
        customer.setSurname(rs.getString(6));

        customer.setSex(rs.getString(7));
        customer.setMaritalStatus(rs.getString(8));

        customer.setCreditRating(rs.getString(9));
        customer.setNABCustomer(Boolean.parseBoolean(rs.getString(10)));

        Address address = new Address();
        address.setId(Long.parseLong(rs.getString(11)));

        address.setCustomerId(Long.parseLong(rs.getString(12)));
        address.setAddressLine1(rs.getString(13));

        address.setAddressLine2(rs.getString(14));

        address.setStreetName(rs.getString(15));
        address.setSuburb(rs.getString(16));

        address.setCity(rs.getString(17));

        address.setState(rs.getString(18));

        address.setCountry(rs.getString(19));


        address.setPinCode(rs.getString(20));
        customer.setAddress(address);
        return customer;
    }


}
