package task;

public interface CRUDRepository {
    String createCustomer(Customer customer);

    void updateCustomer(Customer customer);

    Customer getCustomer(String customerId);

    void deleteUser(String customerId);

    long generateSequence();
}
