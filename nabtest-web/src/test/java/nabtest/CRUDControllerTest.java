package nabtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import task.CRUDService;
import task.CRUDServiceException;
import task.Customer;

import java.io.IOException;

import static org.mockito.Mockito.*;

/**
 * Created by kannan on 19/01/2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ObjectMapper.class,CRUDController.class,CRUDControllerTest.class})
public class CRUDControllerTest {

    public static final String TEST_JSON = "test:json";
    public static final String FAILURE = "Failure";
    public static final String CUST_ID = "custId";

    @Test
    public void testCreateUser() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);


        CRUDController crudController = new CRUDController(mockCRUDService);
        crudController.createUser(TEST_JSON);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);
        verify(mockCRUDService).createCustomer(mockCustomer);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testCreateUserThrowsIOException() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);

        doThrow(new IOException()).when(mockObjectMapper).readValue(TEST_JSON, Customer.class);

        CRUDController crudController = new CRUDController(mockCRUDService);


        ResponseEntity<String> stringResponseEntity = crudController.createUser(TEST_JSON);

        Assert.assertEquals(stringResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        Assert.assertEquals(stringResponseEntity.getBody(), FAILURE);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testCreateUserThrowsCRUDServiceException() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);

        doThrow(new CRUDServiceException(null)).when(mockCRUDService).createCustomer(mockCustomer);

        CRUDController crudController = new CRUDController(mockCRUDService);

        ResponseEntity<String> stringResponseEntity = crudController.createUser(TEST_JSON);

        Assert.assertEquals(stringResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        Assert.assertEquals(stringResponseEntity.getBody(), FAILURE);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);
        verify(mockCRUDService).createCustomer(mockCustomer);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testGetUser() throws CRUDServiceException {
        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        CRUDController crudController = new CRUDController(mockCRUDService);
        crudController.getUser(CUST_ID);
        verify(mockCRUDService).getCustomer(CUST_ID);
        verifyNoMoreInteractions(mockCRUDService);
    }

    @Test
    public void testGetUserThrowsCRUDServiceExeption() throws CRUDServiceException {
        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        CRUDController crudController = new CRUDController(mockCRUDService);
        doThrow(new CRUDServiceException(null)).when(mockCRUDService).getCustomer(CUST_ID);
        ResponseEntity<Customer> customerResponseEntity = crudController.getUser(CUST_ID);
        Assert.assertEquals(customerResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        verify(mockCRUDService).getCustomer(CUST_ID);
        verifyNoMoreInteractions(mockCRUDService);
    }

    @Test
    public void testUpdateUser() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);


        CRUDController crudController = new CRUDController(mockCRUDService);
        crudController.updateUser(TEST_JSON);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);
        verify(mockCRUDService).updateCustomer(mockCustomer);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testUpdateUserThrowsIOException() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);

        doThrow(new IOException()).when(mockObjectMapper).readValue(TEST_JSON, Customer.class);

        CRUDController crudController = new CRUDController(mockCRUDService);


        ResponseEntity<String> stringResponseEntity = crudController.updateUser(TEST_JSON);

        Assert.assertEquals(stringResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        Assert.assertEquals(stringResponseEntity.getBody(), FAILURE);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testUpdateUserThrowsCRUDServiceException() throws Exception, CRUDServiceException {

        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        ObjectMapper mockObjectMapper =Mockito.mock(ObjectMapper.class);
        Customer mockCustomer = mock(Customer.class);
        PowerMockito.whenNew(ObjectMapper.class).withNoArguments().thenReturn(mockObjectMapper);
        when(mockObjectMapper.readValue(TEST_JSON, Customer.class)).thenReturn(mockCustomer);

        doThrow(new CRUDServiceException(null)).when(mockCRUDService).updateCustomer(mockCustomer);

        CRUDController crudController = new CRUDController(mockCRUDService);

        ResponseEntity<String> stringResponseEntity = crudController.updateUser(TEST_JSON);

        Assert.assertEquals(stringResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        Assert.assertEquals(stringResponseEntity.getBody(), FAILURE);

        verify(mockObjectMapper).readValue(TEST_JSON, Customer.class);
        verify(mockCRUDService).updateCustomer(mockCustomer);

        verifyNoMoreInteractions(mockCRUDService,mockObjectMapper,mockCustomer);
    }

    @Test
    public void testDeleteUser() throws CRUDServiceException {
        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        CRUDController crudController = new CRUDController(mockCRUDService);
        crudController.deleteUser(CUST_ID);
        verify(mockCRUDService).deleteUser(CUST_ID);
        verifyNoMoreInteractions(mockCRUDService);
    }

    @Test
    public void tesDeleteUserThrowsCRUDServiceExeption() throws CRUDServiceException {
        CRUDService mockCRUDService = Mockito.mock(CRUDService.class);
        CRUDController crudController = new CRUDController(mockCRUDService);
        doThrow(new CRUDServiceException(null)).when(mockCRUDService).deleteUser(CUST_ID);
        ResponseEntity<String> customerResponseEntity = crudController.deleteUser(CUST_ID);
        Assert.assertEquals(customerResponseEntity.getStatusCode(), HttpStatus.NO_CONTENT);
        verify(mockCRUDService).deleteUser(CUST_ID);
        verifyNoMoreInteractions(mockCRUDService);
    }

}