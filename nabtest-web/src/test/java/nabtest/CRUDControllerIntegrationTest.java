package nabtest;

import org.junit.Test;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;

public class CRUDControllerIntegrationTest {


    private static final String CUSTOMER = "{\"firstName\":\"fname22\",\"middleName\":\"mname3\",\"surname\":\"sname\",\"initials\":\"Mr.\",\"title\":\"Eng.\",\"sex\":\"M\",\"maritalStatus\":\"Married\",\"creditRating\":\"100\",\"address\":{\"id\":0,\"customerId\":0,\"addressLine1\":\"line1\",\"addressLine2\":\"line2\",\"streetName\":null,\"suburb\":null,\"city\":\"city\",\"state\":\"state\",\"country\":\"country\",\"pinCode\":\"12345678\"},\"nabcustomer\":false}";

    public String createUser() throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("data.json", CUSTOMER);

        String result = restTemplate.postForObject("http://localhost:8080/test/create/", map, String.class);
        assertNotNull(result);
        return result;
    }

    @Test
    public void testUpdateUser() throws Exception {

        String id = createUser();

        assertNotNull(id);

        RestTemplate restTemplate = new RestTemplate();

        String user = getUser(id);

        assertNotNull(user);

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("data.json", user);

        String result = restTemplate.postForObject("http://localhost:8080/test/update/", map, String.class);
        assertNotNull(result);

        deleteUser(id);
    }


    public String getUser(String id) throws Exception {

        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject("http://localhost:8080/test/get/"+id,  String.class);
    }

    public void deleteUser(String userId) throws Exception {

        RestTemplate restTemplate = new RestTemplate();

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("data.json", CUSTOMER);
        restTemplate.delete("http://localhost:8080/test/delete/"+userId);

    }

}