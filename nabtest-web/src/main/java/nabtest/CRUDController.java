package nabtest;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import task.CRUDService;
import task.CRUDServiceException;
import task.Customer;

import java.io.IOException;

@RestController
public class CRUDController {

    public static final String DATA_JSON = "data.json";
    private CRUDService crudService;

    @Autowired
    public CRUDController(CRUDService crudService) {

        this.crudService = crudService;

    }

    /**
     *
     * @param customer
     * @return
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> createUser(@RequestParam(DATA_JSON) String customer) {

        ObjectMapper objectMapper = new ObjectMapper();
        Customer cust = null;
        String id = null;
        try {
            cust = objectMapper.readValue(customer, Customer.class);
            id = crudService.createCustomer(cust);

        } catch (IOException e) {
            return new ResponseEntity<String>("Failure", HttpStatus.NO_CONTENT);
        } catch (CRUDServiceException e) {
            return new ResponseEntity<String>("Failure", HttpStatus.NO_CONTENT);

        }
        return new ResponseEntity<String>(id, HttpStatus.OK);
    }

    /**
     *
     * @param customerID
     * @return
     */
    @RequestMapping(value = "/get/{custid}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Customer> getUser(@PathVariable("custid") String customerID) {


        Customer customer1 = null;
        try {
            customer1 = crudService.getCustomer(customerID);
        } catch (CRUDServiceException e) {
            return new ResponseEntity<Customer>(customer1, HttpStatus.NO_CONTENT);

        }

        return new ResponseEntity<Customer>(customer1, HttpStatus.OK);
    }

    /**
     *
     * @param customer
     * @return
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> updateUser(@RequestParam(DATA_JSON) String customer) {


        ObjectMapper objectMapper = new ObjectMapper();
        Customer cust = null;
        try {
            cust = objectMapper.readValue(customer, Customer.class);
            crudService.updateCustomer(cust);
        } catch (IOException e) {
            return new ResponseEntity<String>("Failure", HttpStatus.NO_CONTENT);
        } catch (CRUDServiceException e) {
            return new ResponseEntity<String>("Failure", HttpStatus.NO_CONTENT);
        }


        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

    /**
     *
     * @param customer
     * @return
     */
    @RequestMapping(value = "/delete/{custid}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> deleteUser(@PathVariable("custid") String customer) {

        try {
            crudService.deleteUser(customer);
        } catch (CRUDServiceException e) {
            return new ResponseEntity<String>("Failure", HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }

}
